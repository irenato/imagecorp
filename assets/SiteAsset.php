<?php

namespace frontend\assets;

use yii\web\AssetBundle;


/**
 * Main frontend application asset bundle.
 */
class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/font-awesome.css',
        'theme/css/style.css',

    ];
    public $js = [
        'js/scripts.js',
        'js/cookie.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'frontend\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
