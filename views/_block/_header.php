<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

/**
 * Created by IntelliJ IDEA.
 * User: sandro
 * Date: 29.04.16
 * Time: 13:51
 */
?>
<div class="header-area">
    <header class="m-header">
        <div class="<?= (Yii::$app->controller->route == 'site/index' || Yii::$app->controller->route == 'site/my-tests' || Yii::$app->controller->route == 'test/result')? 'blog-link':'blog-link style-grey'; ?>">
            <a class="" href="<?= Url::toRoute(['/blog/index']); ?>">Блог</a>
        </div>
        <div class="logo">
            <a href="/"><img src="<?= (Yii::$app->controller->route == 'site/index' || Yii::$app->controller->route == 'site/my-tests' || Yii::$app->controller->route == 'test/result')? '/theme/img/logo_light.svg':'/theme/img/logo_dark.svg'; ?>" alt="Корпорация Имиджа"></a>
        </div>
            <?php
                if(!Yii::$app->user->isGuest){
                    echo '<ul class="user-menu main-page">';
                    echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['dashboard/index']).'">Профиль</a></li>';
                    echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['site/my-tests']).'">Мои тесты</a></li>';
                   echo '<li>' . $url = Html::a('Выйти',
                       ['//user/security/logout'],
                       ['data-method'=>'post']) . '</li>';
//                    echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['user/security/logout']).'">Выйти</a></li>';
                    echo '</ul>';
                    echo '<div class="header-right header-menu m-hidden">';
                    echo '<div class="header-item">';
                    echo '<img src="/' . Yii::getAlias('@avatar') . '/' . Yii::$app->user->identity->avatar . '" alt="">';

                    if(Yii::$app->controller->route == 'site/index' || Yii::$app->controller->route == 'site/my-tests' || Yii::$app->controller->route == 'test/result'){
                        echo '<p class="login-btn">' . Yii::$app->user->identity->username . '</p>';
                    }
                    else{
                        echo '<p class="login-btn style-color">' . Yii::$app->user->identity->username . '</p>';
                    }
                    
                    echo '</div>';
                }
                else{
                    echo '<ul class="user-menu main-page">';
                    echo '<li><a data-toggle="modal" data-target="#login" href="#">Войти</a></li>';
//                    echo '<li><a href="' .  Yii::$app->getUrlManager()->getBaseUrl() . 'user/security/login ">Войти</a></li>';
                    echo '<li><a data-toggle="modal" data-target="#registration" href="#">Зарегистрироваться</a></li>';
//                    echo '<li><a href="' .  Yii::$app->getUrlManager()->getBaseUrl() . 'user/registration/register ">Зарегистрироваться</a></li>';
                    echo '</ul>';
                    echo '<div class="header-right header-menu m-hidden">';
                    echo '<div class="header-item">';
//                    if(Yii::$app->controller->route == 'site/index'){
                        echo '<p class="login-btn">Меню</p>';
//                    }
//                    else{
//                        echo '<p class="login-btn style-color">Меню</p>';
//                    }
                    echo '</div>';
                }
            ?>
        </div>
        <div class="hidden-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </header>
</div>
