//------------------------------
//dropzone for upload avatar
// into dashboard
//------------------------------
$(document).ready(function () {
    var dropzone = $('#dropzone'),
        img_url = $("#user-avatar").attr('src');

    //------------------------------
    //upload files
    //------------------------------
    var upload = function (files) {
        var formData = new FormData();
        formData.append('file', files[0]);
        var request = $.ajax({
            data: formData,
            url: '/dashboard/upload-avatar',
            type: 'post',
            dataType: 'html',
            contentType: false,
            processData: false,
        });

        request.done(function (url) {
            $("#user-avatar").attr('src', url);
            $("form.user-change input[name='EditProfileForm[avatar]']").val(url);
            // $(dropzone).css('background', 'url(' + url + ')');
            img_url = url;
        });

        request.fail(function (jqXHR, textStatus) {
            alert(textStatus);
        });

    };

    dropzone.ondrop = function (e) {
        e.preventDefault();
        var img = $("#dropzone1").find("img");
        if (img.length != 1) {
            this.className = 'dropzone';
            upload(e.dataTransfer.files);
        }
    };

    dropzone.ondragover = function () {
        this.className = 'dropzone dragover';
        return false;
    };

    dropzone.ondragleave = function () {
        this.className = 'dropzone';
        return false;
    };
    $(".dropzone").click(function () {
        $('#open_browse').trigger('click');
    });

    $(document).on('click', 'label.avatar-label', function (e) {
        e.preventDefault();
        $('#open_browse').trigger('click');
    })
    $('#open_browse').change(function () {
        var file = this.files;
        if (file.length == 1)
            upload(file);
    })

    //------------------------------------------------------
    // add comment
    //------------------------------------------------------
    $('body').on('click', '#add-comment', function (e) {
        $('#error-from-comment').text('');
        $('#success-from-comment').text('');
        e.preventDefault();
        var comment = $('#add-comment-text');
        if (comment.val() == '') {
            $('#error-from-comment').text('Напишите комментарий');
        } else {
            var request = $.ajax({
                data: {
                    'comment': comment.val(),
                },
                url: '/dashboard/add-comment',
                type: 'post',
                dataType: 'html',
            });
            request.done(function (result) {
                if (result == 'Done!') {
                    $('#success-from-comment').text('Ваш коментарий успешно добавлен');
                    comment.val('');
                }
            })
        }
    })

    //------------------------------------------------------
    // заявка на выод средств
    //------------------------------------------------------
    var income_sum1 = $('#income-1'),
        income_sum2 = $('#income-2'),
        tests_ids = $('#income-1').attr('data-income');
    $('body').on('click', '#get-my-income', function (e) {
        $('#error-from-payment').text('');
        $('#success-from-payment').text('');
        e.preventDefault();
        if (income_sum1.val() !== income_sum2.val()) {
            $('#error-from-payment').text('Ошибка!');
        } else if (income_sum1.val() < 500) {
            e.preventDefault();
            $('#error-from-payment').text('Сумма должна быть не менее 500');
        }
        // else {
        //     var request = $.ajax({
        //         data: {
        //             'tests_ids': tests_ids,
        //         },
        //         url: '/dashboard/create-proposal',
        //         type: 'post',
        //         dataType: 'html',
        //     });
        //     request.done(function (result) {
        //         if (result == 'Done!') {
        //             $('#success-from-payment').text('Ваша заявка принята успешно');
        //             setTimeout(location.reload(), 5000);
        //         } else {
        //             $('#success-from-payment').text(result);
        //         }
        //     });
        // }
    });

    $('body').on('click', '#refer-payment-button', function (e) {
        $('#form-refer-payment span.text-danger').text('');
        $('#form-refer-payment span.text-success').text('');
        e.preventDefault();
        var first_name = $("#form-refer-payment input[name='first_name']"),
            last_name = $("#form-refer-payment input[name='last_name']"),
            phone_number = $("#form-refer-payment input[name='phone_number']"),
            requisites = $("#form-refer-payment textarea[name='requisites']");
        if (first_name.val() == '' || last_name.val() == '' || phone_number.val() == '' || requisites.val() == '') {
            $('#form-refer-payment span.text-danger').text('Заполните все поля!');
        } else {
            var request = $.ajax({
                data: {
                    'tests_ids': tests_ids,
                    'first_name': first_name.val(),
                    'last_name': last_name.val(),
                    'phone_number': phone_number.val(),
                    'requisites': requisites.val(),
                },
                url: '/dashboard/create-proposal',
                type: 'post',
                dataType: 'html',
            });
            request.done(function (action) {
                if (action == 'Done!') {
                    $('#form-refer-payment span.text-success').text('Ваша заявка успешно оформлена!');
                    setTimeout(location.reload(), 3000);
                }
            })
        }
    })

//------------------------------
//edit user data
//------------------------------
//     $("form.user-change input[name='email']").val('');
//     $("form.user-change input[name='password']").val('');
//     $("form.user-change input[name='password2']").val('');
//     $(document).on('click', 'form.user-change button', function (e) {
//         e.preventDefault();
//         var username = $("form.user-change input[name='username']"),
//             email = $("form.user-change input[name='email']"),
//             password = $("form.user-change input[name='password']"),
//             password2 = $("form.user-change input[name='password2']"),
//             formData = $('form.user-change').serializeArray(),
//             send_data = $.ajax({
//                 data: {
//                     formData: formData,
//                     avatar: img_url,
//                 },
//                 url: '/dashboard/edit-profile',
//                 type: 'post',
//                 dataType: 'html'
//             });
//     })
})
