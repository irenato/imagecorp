<?php

namespace frontend\widgets;

use Yii;
use dektrium\user\models\User;
use yii\web\NotFoundHttpException;
use dektrium\user\models\ResendForm;
use dektrium\user\models\RegistrationForm;
use common\models\main\DataOrder;
use yii\filters\AccessControl;
use frontend\controllers\SiteController;
use frontend\models\Refer;

class RegistrationWidget extends \yii\bootstrap\Widget
{
    public function init()
    {
    }

    public function run()
    {
        $session = Yii::$app->session;
        $form_model = new RegistrationForm;
        if ($form_model->load(Yii::$app->request->post()) && $form_model->validate() && $form_model->register()) {
            $user = User::findOne(['email' => $form_model->email]);
            Yii::$app->user->login($user);
            if(isset($session['partner_id'])){
                $refer = new Refer();
                $refer->addNewUserIdToPartner(Yii::$app->user->identity->id, $session['partner_id']);
            }
            SiteController::refresh();
        }
        return $this->render('registration/view', [
            'form_model' => $form_model,
        ]);
    }
}