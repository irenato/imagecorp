<?php

/* @var $this yii\web\View */

/**
 * Created by IntelliJ IDEA.
 * User: sandro
 * Date: 29.04.16
 * Time: 14:15
 */
?>
<footer class="footer bg-dark">
    <div class="container-main">
        <div class="logo-footer"><img src="/theme/img/logo_light.svg" alt=""></div>

        <ul class="footer-nav">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['blog/index']) ?>">Блог</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['site/wedd-test']) ?>">Свадьбы</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['site/list-test']) ?>">Тесты</a></li>
        </ul>

        <div class="social-links">
            <a href="https://www.facebook.com"><img src="/theme/img/fb_footer.svg" alt=""></a>
            <a href="http://vk.com/imagecorp"><img src="/theme/img/vk_footer.svg" alt=""></a>
            <a href="https://twitter.com/imagecorp?s=09"><img src="/theme/img/tw_footer.svg" alt=""></a>
            <a href="https://www.youtube.com/channel/UCsEOsGrVbvMJvfqgN-vJCtA"><img src="/theme/img/yt_footer.svg" alt=""></a>
        </div>
    </div>
    <p class="copyrights">&copy; 2016 КОРПОРАЦИЯ ИМИДЖА. Онлайн-сервис по подбору имиджа. </p>
</footer>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter37685560 = new Ya.Metrika({
                    id:37685560,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/37685560" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

