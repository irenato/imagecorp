<?php

use common\models\database\Gallery;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?= $this->render('/_block/_header_payment'); ?>

<!-- user profile-->
<div class="user-profile">
    <div class="container-main">
        <aside class="user-settings">
            <div class="head-wrap">
                <div class="wrap-avatar">
                    <img id="user-avatar"
                         src="<?= Yii::getAlias('@avatar') . '/' . Yii::$app->user->identity->avatar ?>" alt="avatar">
                </div>
                <input accept="image/*;capture=camera" style="display : none;" id="open_browse"
                       value="<?= Yii::$app->request->getCsrfToken() ?>" type="file">
                <div class="dropzone" id="dropzone">
                </div>
                <label for="avatar-img" class="avatar-label"></label>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'user-change', 'options' => ['class' => 'user-change',]]); ?>
            <!--            <form action="#" class="user-change">-->
            <?= $form->field($model, 'username', [
                'inputOptions' => [
                    'placeholder' => 'Имя',
                    'value' => Yii::$app->user->identity->username
                ],
            ])->label(false); ?>
            <?php if (strstr(Yii::$app->user->identity->email, 'default')) : ?>
                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => 'E-mail',
                    ],
                ])->label(false); ?>
            <?php else : ?>
                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => 'E-mail',
                        'value' => Yii::$app->user->identity->email
                    ],
                ])->label(false); ?>
            <?php endif; ?>
            <?= $form->field($model, 'password', [
                'inputOptions' => [
                    'placeholder' => 'Новый пароль',
                ],
            ])->label(false); ?>
            <?= $form->field($model, 'password_repeat', [
                'inputOptions' => [
                    'placeholder' => 'Подтвердите пароль',
                ],
            ])->label(false); ?>
            <!--                <input name="username" type="text" placeholder="-->
            <? //= Yii::$app->user->identity->username ?><!--">-->
            <!--                <input name="email" type="email" placeholder="Новый E-mail" autocomplete="off">-->
            <!--                <input name="password" type="password" placeholder="Новый пароль" val="" autocomplete="off">-->
            <!--                <input name="password2" type="password" placeholder="Подтвердите пароль" val="" autocomplete="off">-->
            <?= $form->field($model, 'avatar', [
                'inputOptions' => [
                    'value' => Yii::getAlias('@avatar') . '/' . Yii::$app->user->identity->avatar
                ],
            ])->hiddenInput()->label(false); ?>
            <?= Html::submitButton('Сохранить') ?>
            <!--                <button type="submit">СОХРАНИТЬ</button>-->
            <?php ActiveForm::end() ?>
            <!--            </form>-->
        </aside>
        <div class="main-part">
            <div class="top-part">
                <h2 class="section-title">У "Корпорации Имиджа" найдется все</h2>
                <p class="section-text">Партнерская программа для тех, кому мы стали интересны. Вы прошли тест, стало
                    интересно, понравились ответы. Решили, что готовы посоветовать сайт знакомой(му). Вперед.</p>
            </div>
            <div class="center-part">
                <div class="middle-col">
                    <p class="section-text">Мы генерировали персональную реферальную ссылку Нажмите на "Copy" и
                        отправляйте друзьям свою ссылку, получая за каждый пройденный тест фиксированные 15%. </p>

                    <input readonly=true type="text" class="copy-link"
                           value="http://imagecorp.ru?<?= Yii::$app->user->identity->username ?>">

                    <ul class="test-list">
                        <?php if ($tests) : ?>
                            <?php foreach ($tests as $test) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['test/inittest', 'id' => $test['id']]) ?>">
                                        <div class="wrap-img list-item-9" style="background-image: url(<?= Gallery::findOne(['id' => $test['img']])->src ?>); background-size: cover;"></div>
                                        <p class="test-title"><?=  $test['title'] . ' ' . $test['price']?>р</p>
                                        <p class="price"><?= (int)$test['price'] * 0.15 ?>р</p>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-1"></div>-->
                        <!--                                <p class="test-title">Цветотип женщины 399р</p>-->
                        <!--                                <p class="price">59,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-2"></div>-->
                        <!--                                <p class="test-title">Типы фигуры женщины 499р</p>-->
                        <!--                                <p class="price">74,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-3"></div>-->
                        <!--                                <p class="test-title">Стиль одежды женщины 599р</p>-->
                        <!--                                <p class="price">89,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-4"></div>-->
                        <!--                                <p class="test-title">Тип лиц женщины 59р</p>-->
                        <!--                                <p class="price">8,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-5"></div>-->
                        <!--                                <p class="test-title">Комплекс женщины 1399р</p>-->
                        <!--                                <p class="price">209,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-9"></div>-->
                        <!--                                <p class="test-title">Свадебные стили мужчины 499р</p>-->
                        <!--                                <p class="price">67,35р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-1"></div>-->
                        <!--                                <p class="test-title">Цветотип мужчины 249р</p>-->
                        <!--                                <p class="price">37,35р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-6"></div>-->
                        <!--                                <p class="test-title">Типы фигуры мужчины 449р</p>-->
                        <!--                                <p class="price">67,35р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-3"></div>-->
                        <!--                                <p class="test-title">Стиль одежды мужчины 549р</p>-->
                        <!--                                <p class="price">74,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-7"></div>-->
                        <!--                                <p class="test-title">Тип лиц мужчины 59р</p>-->
                        <!--                                <p class="price">8,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-8"></div>-->
                        <!--                                <p class="test-title">Комплекс мужчины 1399р</p>-->
                        <!--                                <p class="price">209,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                        <!---->
                        <!--                        <li>-->
                        <!--                            <a href="#">-->
                        <!--                                <div class="wrap-img list-item-9"></div>-->
                        <!--                                <p class="test-title">Свадебные стили комплекс 1099р</p>-->
                        <!--                                <p class="price">164,85р</p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->

                    </ul>

                    <p class="section-text">Корпорация Имиджа не только позволяет узнать человеку о себе, о имидже.
                        Кому-то она способна помочь зарабатывать на партнерской программе первые деньги в Интернет.</p>

                    <form action="#" class="comment-form">
                        <span class="form-title">Отзыв</span>
                        <textarea name="textarea" id="add-comment-text"></textarea>
                        <div><span id="error-from-comment" class="text-danger"></span></div>
                        <div><span id="success-from-comment" class="text-success"></span></div>
                        <div class="btn-wrap">
                            <button id="add-comment" type="submit" class="submit-btn">ОТПРАВИТЬ</button>
                        </div>
                    </form>
                </div>
                <aside class="purse-sidebar">

                    <p class="disc">К примеру: тест стоит 1 000 рублей. Если человек, который пришел по Вашей ссылке на
                        сайт прошел и оплатил, то Вам будет начислено 150 рублей. Выводить же средства Вы имеете право
                        на любую платежную систему, представленную в нашем сервисе. </p>

                    <span class="purse-label">Приглашенные </span>
                    <input type="text" value="<?= $fetch_count ?>" readonly>

                    <span class="purse-label">На Вашем счетy <span class="currency rub"></span></span>
                    <input id="income-1" class="no-marg" data-income="<?= implode(',', $my_fetch_users_finished_tests_id) ?>" type="text" value="<?= $income ?>" readonly>

                    <span class="purse-label">Вывод <span class="currency rub"></span></span>
                    <input id="income-2" type="number" min="10" value="<?= $income ?>">
                    <div><span id="error-from-payment" class="text-danger"></span></div>
                    <div><span id="success-from-payment" class="text-success"></span></div>
                    <button id="get-my-income" type="button" class="purse-btn" <?= $income >= 500 ? "data-toggle='modal' data-target='#refer-payment'" : '' ?>>ОТПРАВИТЬ</button>
<!--                    <button id="get-my-income" type="button" class="purse-btn">ОТПРАВИТЬ</button>-->
                </aside>
            </div>
        </div>
    </div>
</div>
<!-- end user profile-->
<?= $this->render('/_block/_footer'); ?>
