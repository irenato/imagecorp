<?php
//use Yii;
$this->title = 'Корпорация Имиджа | Тест';

use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="container">
    <nav class="navbar">
        <!-- Left Side -->
        <div class="navbar-item">
            <span class="icon is-medium"><a href="https://twitter.com/imagecorp?s=09"><i class="fa fa-twitter"></i></a></span>
            <span class="icon is-medium"><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></span>
            <span class="icon is-medium"><a href="http://vk.com/imagecorp"><i class="fa fa-vk"></i></a></span>
            <span class="icon is-medium"><a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a></span>
        </div>

        <!-- /Left Side -->
        <!-- Center -->
        <div class="navbar-item is-text-centered">
            <a href="/"><img src="<?= Yii::$app->params['site']; ?>/theme/img/logo.svg" class="logo-svg" alt="Корпорация Имиджа"></a>
        </div>
        <!-- /Center -->
        <!-- Right Side -->
        <?php
        if(!Yii::$app->user->isGuest){
            echo '<ul class="user-menu">';
            echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['dashboard/index']).'">Профиль</a></li>';
            echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['site/my-tests']).'#">Мои тесты</a></li>';
            echo '<li>' . $url = Html::a('Выйти',
                ['//user/security/logout'],
                ['data-method'=>'post']) . '</li>';
//                    echo '<li><a href="' .  Yii::$app->urlManager->createUrl(['user/security/logout']).'">Выйти</a></li>';
            echo '</ul>';
            echo '<div class="header-right header-menu m-hidden">';
            echo '<div class="header-item">';
            echo '<img src="/' . Yii::getAlias('@avatar') . '/' . Yii::$app->user->identity->avatar . '" alt="">';

            if(Yii::$app->controller->route == 'site/index'){
                echo '<p class="login-btn">' . Yii::$app->user->identity->username . '</p>';
            }
            else{
                echo '<p class="login-btn style-color">' . Yii::$app->user->identity->username . '</p>';
            }

            echo '</div>';
        }
        else{
            echo '<ul class="user-menu">';
            echo '<li><a data-toggle="modal" data-target="#login" href="#">Войти</a></li>';
//                    echo '<li><a href="' .  Yii::$app->getUrlManager()->getBaseUrl() . 'user/security/login ">Войти</a></li>';
            echo '<li><a data-toggle="modal" data-target="#registration" href="#">Зарегистрироваться</a></li>';
//                    echo '<li><a href="' .  Yii::$app->getUrlManager()->getBaseUrl() . 'user/registration/register ">Зарегистрироваться</a></li>';
            echo '</ul>';
            echo '<div class="header-right header-menu m-hidden">';
            echo '<div class="header-item">';
            if(Yii::$app->controller->route == 'site/index'){
                echo '<p class="login-btn">Меню</p>';
            }
            else{
                echo '<p class="login-btn style-color">Меню</p>';
            }
            echo '</div>';
        }
        ?>
<!--        <div class="navbar-item is-text-right accent normal text_1">-->
<!--            <a href="#">Войти</a> или-->
<!---->
<!--            <a href="#">Зарегистрироваться</a>-->
<!--        </div>-->
        <!-- /Right Side -->

    </nav>
</div>