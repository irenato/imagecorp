<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 10.08.2016
 * Time: 15:15
 */

namespace frontend\widgets;

use frontend\controllers\SiteController;
use Yii;
use dektrium\user\models\RecoveryForm;

class RecoveryWidget extends \yii\bootstrap\Widget
{
    public function init()
    {
    }

    public function run()
    {

//        if ($model->load(Yii::$app->getRequest()->post()) && $model->sendRecoveryMessage()) {
//            SiteController::refresh();
//        }
        return $this->render('recovery/view');
    }
}