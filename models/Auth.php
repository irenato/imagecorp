<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 02.08.2016
 * Time: 20:14
 */

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use dektrium\user\models\User;

/**
 * This is the model class for table "auth".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $source_id
 *
 * @property User $user
 */
class Auth extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'source', 'id_source'], 'required'],
            [['id_user'], 'integer'],
            [['source', 'id_source'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'User ID',
            'source' => 'Source',
            'id_source' => 'Source ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}


