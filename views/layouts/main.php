<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\SiteAsset;
use yii\bootstrap\BootstrapAsset;

SiteAsset::register($this);
//$this->title = 'Корпорация Имиджа';
//?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://imagecorp.ru/theme/img/Корпорация имиджа.png"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta name="yandex-verification" content="aa951462f21dba8f"/>
</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
<?php if(Yii::$app->controller->route !== 'dashboard/index') : ?>

<?php $this->registerJs("
    $('body').on('show.bs.modal', '#login', function () {
        $('#registraion').modal('hide');
        $('#recovery').modal('hide');
    });
    $('body').on('click', '#callloginmodal', function () {
        $('#registration').modal('hide');
        $('#recovery').modal('hide');
    });
   $('body').on('show.bs.modal', '#recovery', function () {
        $('#login').modal('hide');
        $('#registraion').modal('hide');
    });
    
    var message='';
    function clickIE() {if (document.all) {(message);return false;}}
    function clickNS(e) {if
    (document.layers||(document.getElementById&&!document.all)) {
    if (e.which==2) {
    (message);
    return false;}}}
    if (document.layers) {
    document.captureEvents(Event.MOUSEDOWN);
    document.onmousedown=clickNS;
    }else{
    document.onmouseup=clickNS;
    document.oncontextmenu=clickIE;
    }
    document.oncontextmenu=new Function('return false')
     $('body').bind('copy paste', function(e) {
        e.preventDefault();
    });
    "); ?>
<?php endif; ?>
<?= frontend\widgets\RegistrationWidget::widget() ?>
<?= frontend\widgets\LoginWidget::widget() ?>
<?= frontend\widgets\RecoveryWidget::widget() ?>
<?= frontend\widgets\ReferPaymentWidget::widget() ?>
</body>
</html>
<?php $this->endPage() ?>
