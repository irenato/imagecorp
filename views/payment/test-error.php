<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 11.08.2016
 * Time: 20:07
 */

?>

<?= $this->render('/_block/_header_payment'); ?>

<div class="container">
    <section class="hero">
        <div class="hero-content">
            <!-- <h1 class="title">CЛЕДУЮЩИЙ ТЕСТ</h1> -->
            <div class="breadcrumbs is-text-centered">
                <div class="is-ib">
                    <p>ВОЗНИКЛА ОШИБКА</p>
                </div>
            </div>
        </div>
    </section>
    <section>

        <div class="columns">

            <div class="is-third center-block">
                <div class="card">
                    <div class="card-image">
                        <figure class="relative image  card-img-size">
                            <img src="/theme/img/цветотип.jpg" class="blogIndexImg" alt="">
                            <!-- <div class="is-overlay prices">
                                <del>399</del> 299р
                            </div> -->
                            <a class="is-overlay pink-hover" href="#">
                                <!-- ПРОЙТИ ТЕСТ -->
                            </a>
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <span class="card-prim">ОШИБКА!</span>
                            </div>
                            <div class="media-content is-text-right">
                            </div>

                        </div>
                        <div>
                            Приносим свои извенения. К сожалению, на данный тест еще не создан ответ.
                            Сейчас Вы будете перенаправлены на страницу с выбором тестов.
                        </div>
                        <div class="big-price">
                        </div>
                        <div class="is-text-centered">

                            <!--                                <a href="-->
                            <? //= Yii::$app->urlManager->createUrl(['/site/pay-system']) ?><!--" class="button primary primary_min" type="submit">Оплатить</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--        --><?php //endif; ?>
    </section>