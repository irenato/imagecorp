<?php

use yii\helpers\Url;
use common\models\database\Discount;
use common\models\database\Gallery;
?>

<section class="test-list">
    <div class="container-main">
        <h2 class="section-title">Тесты</h2>
        <ul class="list-of-tests">
            <?php
            $i = 0;
            foreach ($tests as $item) {
                echo '<li>';
                echo '<div class="test-img list-item list-item-' . $i . '" style="background-image: url(' . Gallery::findOne(['id' => $item->img])->src . ');">';
                echo '<div class="test-price">';
                if (Discount::findOne(['id_test' => $item->id, 'status' => 1])) {
                    echo '<span class="old-price">' . $item->price . '</span>';
                    echo ' ';
                    echo '<span class="new-price">' . Discount::findOne(['id_test' => $item->id, 'status' => 1])->amount . '</span>';
                } else {
                    echo '<span class="new-price">' . $item->price . '</span>';
                }
                echo '<span class="currency-price">p</span>';
                echo '</div>';
                echo '<a href="'.Url::toRoute(['test/inittest', 'id' => $item->id]).'" class="btn-go-test">ПРОЙТИ ТЕСТ</a>';
                echo '</div>';
                echo '<div class="disk-test">';
                echo '<h3 class="list-title">' . $item->title . '</h3>';
                echo '<p class="likes"><span class="likes-count">' . $item->like . '</span></p>';
                echo '</div>';
                echo '</li>';
                $i++;
            }
            ?>
        </ul>
        <a href="<?= Url::toRoute(['/site/list-test']); ?>" class="btn-more-test">БОЛЬШЕ ТЕСТОВ</a>
    </div>
</section>
