<?php

namespace frontend\controllers;

use frontend\models\Refer;
use frontend\models\TestResults;
use frontend\models\ReferIncome;
use frontend\models\UserComments;
use frontend\models\PaymentRequisites;
use Yii;
use common\models\database\Test;
use common\models\database\Order;
use frontend\models\EditProfileForm;
use dektrium\user\models\User;
use dektrium\user\helpers\Password;

class DashboardController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->isGuest) {
            Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
            Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
            Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js', ['depends' => 'yii\web\YiiAsset']);
            Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
            Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
            Yii::$app->view->registerJsFile('js/dashboard.js', ['depends' => 'frontend\assets\AppAsset']);
            $model = new EditProfileForm();
            $refer = new Refer();
            $fetch_count = $refer->showFetchCount();
            $my_fetch_users_ids = $refer->showMyFetchUsersId() ? array_column($refer->showMyFetchUsersId(), 'id_newuser') : [];
            $temp_fetch_users_finished_tests = $this->selectFinishedTestsIdsForMyUsers($my_fetch_users_ids);
            $my_fetch_users_finished_tests_id = $temp_fetch_users_finished_tests ? array_column($temp_fetch_users_finished_tests, 'id_test') : [];
            $my_fetch_users_total_sum = $temp_fetch_users_finished_tests ? array_column($temp_fetch_users_finished_tests, 'income') : [];
            unset($temp_fetch_users_finished_tests_id);
//            $my_fetch_users_finished_tests = $this->selectFinishedTestsForMyUsers($my_fetch_users_finished_tests_id);
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->editUserData();
            }
            return $this->render('index', [
                'model' => $model,
                'tests' => $this->selectAllTests(),
                'fetch_count' => $fetch_count,
                'income' => array_sum($my_fetch_users_total_sum) * 0.15,
                'my_fetch_users_finished_tests_id' => $my_fetch_users_finished_tests_id,
            ]);
        } elseif ($request->get('token') != null) {
            $user = User::findOne(['auth_key' => $request->get('token')]);
            Yii::$app->user->login($user);
            return $this->redirect(['dashboard/index']);
        } else {
            return $this->redirect(['site/index']);
        }
    }

    public function actionUploadAvatar()
    {
        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);
            if ($temp[1] == 'png' || $temp[1] == 'jpeg' || $temp[1] == 'jpg' || $temp[1] == 'bmp') {
                $ext = '.' . $temp[1];
                $name = time();
                move_uploaded_file($file['tmp_name'], Yii::getAlias('@avatar') . '/' . $name . $ext);
                echo Yii::getAlias('@avatar') . '/' . $name . $ext;
            }
        }
    }

    private function selectFinishedTestsIdsForMyUsers($users_ids)
    {
        return ReferIncome::find()
            ->select(['id_test', 'income'])
            ->where(['in', 'id_newuser', $users_ids])
            ->andWhere(['status' => 0])
            ->asArray()
            ->all();
    }

    private function selectAllTests()
    {
        return Test::find()
            ->select(['id', 'title', 'img', 'price'])
            ->orderBy('price DESC')
            ->asArray()
            ->all();
    }

    private function selectFinishedTestsForMyUsers($tests_ids)
    {
        return Test::find()
            ->where(['in', 'id', $tests_ids])
            ->asArray()
            ->all();
    }

    public function actionRecoveryAccount()
    {
        $request = Yii::$app->request;
        $user = User::findOne(['email' => $request->post('user_mail')]);
        if (!$user) {
            return "Error!";
        } else {
            $to = $user->email;
            $subject = '=?UTF-8?B?' . base64_encode('Восстановление пароля') . '?=';
            $message = iconv("utf-8", "windows-1251", 'Для возврата в личный кабинет с возможностью изменения пароля, перейдите по ссылке: http://imagecorp.ru/dashboard/index/*?token=' . $user->auth_key);
//            $message = '=?UTF-8?B?'.base64_encode('Для возврата в личный кабинет с возможностью изменения пароля, перейдите по ссылке: http://imagecorp.ru/test/test/*?token=' . $user->auth_key) . '?=';
            $headers = 'From: imagecorp' . "\r\n" .
                'Reply-To: imagecorp' . "\r\n";
            mail($to, $subject, $message, $headers);
            return "Done!";
        }
    }

    public function actionCreateProposal()
    {
        $request = Yii::$app->request;
        if ($request->post('tests_ids') != null) {
            $tests_ids = explode(',', $request->post('tests_ids'));
            $payment_requisites = new PaymentRequisites();
            if (ReferIncome::updateAll(['status' => 1], ['and', ['id_partner' => Yii::$app->user->identity->id], ['in', 'id_test', $tests_ids]]) &&
                $payment_requisites->addNewRequisites($request->post('first_name'), $request->post('last_name'), $request->post('phone_number'), $request->post('requisites')))
                return 'Done!';
        }
    }

    public function actionConfirmApplication()
    {
        return 'fsdf!';
    }

    public function actionAddComment()
    {
        $request = Yii::$app->request;
        $comment = new UserComments();
        if ($comment->addComment($request->post('comment')))
            echo 'Done!';
    }

    public function actionLoginUser()
    {
        $request = Yii::$app->request;
        $user = User::findOne(['username' => $request->post('username')]);
        if ($user && Password::validate($request->post('password'), $user->password_hash)) {
            Yii::$app->user->login($user, 3600 * 24 * 30);
            $data['message'] = null;
            $data = json_encode($data);
            return $data;
        } else {
            $data['message'] = 'Неправильный логин или пароль';
            $data = json_encode($data);
            return $data;
        }
    }

    /*    public function actionEditProfile()
        {
            $request = Yii::$app->request;
            $form_data = $request->post('formData');
            $avatar = array_pop(explode('/', $request->post('avatar')));
            var_dump($avatar);
        }*/

}