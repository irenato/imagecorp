<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<!--login modal-->
<div class="modal fade" tabindex="-1" role="dialog" id="login"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="login-modal" style="display: block">
        <div class="modal-content">

            <form id="login-form" action="#">
                <input type="text" name="username" placeholder="Имя">
                <div><span class="text-danger login-username"></span></div>
                <input type="password" name="password" placeholder="Пароль">
                <div><span class="text-danger login-password"></span></div>
                <button id="login-button" class="no-marg" type="submit">ВОЙТИ</button>
            </form>

<!--            --><?php //$form = ActiveForm::begin([
//                'id' => 'login-form',
//                'enableAjaxValidation' => true,
//                'enableClientValidation' => true,
//                'validateOnBlur' => true,
//                'validateOnType' => true,
//                'validateOnChange' => true,
//            ]) ?>
<!--            --><?//= $form
//                ->field($login_model, 'login', [
//                    'inputOptions' => [
//                        'placeholder' => 'Имя',
//                    ],
//                ])
//                ->label(false) ?>
<!--            --><?//= $form
//                ->field($login_model, 'password', [
//                    'inputOptions' => [
//                        'placeholder' => 'Пароль',
//                    ],
//                ])
//                ->label(false)->passwordInput()  ?>
<!--            --><?//= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
<!--            --><?php //ActiveForm::end(); ?>
            <span class="info-text">C помощью аккаунта в соц. сетях</span>

            <ul class="login-by-soc">
                <li><a class="soc-item" href="/site/auth/*?authclient=twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a class="soc-item" href="/site/auth/*?authclient=facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a class="soc-item" href="/site/auth/*?authclient=vk"><i class="fa fa-vk"></i></a></li>
            </ul>

            <span class="info-text"><a data-toggle="modal" data-target="#recovery" href="#">Восстановить пароль</a></span>
<!--            <span class="info-text"><button class="recover-pass" type="button">Восстановить пароль</button></span>-->
        </div>
    </div>
</div>
<!--end login modal-->
