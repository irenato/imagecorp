<?php

?>

<div class="modal fade" tabindex="-1" role="dialog" id="refer-payment"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="register-modal" style="display: block">
        <div class="modal-content">
            <form id="form-refer-payment" action="#">
                <input type="text" name="first_name" placeholder="Имя">
                <input type="text" name="last_name" placeholder="Фамилия">
                <input type="text" name="phone_number" placeholder="Номер телефона">
                <textarea name="requisites" placeholder="Реквизиты к оплате"></textarea>
                <div><span class="text-danger"></span></div>
                <div><span class="text-success"></span></div>
                <button id="refer-payment-button" class="no-marg" type="submit">ОТПРАВИТЬ</button>
            </form>
        </div>
    </div>
</div>
<!-- end register modal -->
