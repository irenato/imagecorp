<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 05.08.2016
 * Time: 14:48
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use dektrium\user\models\User;
use dektrium\user\helpers\Password;

class EditProfileForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $avatar;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username rules
            ['username', 'string', 'min' => 3, 'max' => 255, 'message' => "Минимальная длина 3 символов"],
            ['username', 'filter', 'filter' => 'trim', 'message' => 'Поле "Имя" не может быть пустым!'],
            ['username', 'required'],

            // email rules
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email', 'message' => "Неверный формат email"],

            // password rules
            ['password', 'string', 'min' => 6, 'message' => "Минимальная длина пароля - 6 символов"],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают!"],

            // password rules
            ['avatar', 'filter', 'filter' => 'trim'],
            ['avatar', 'string', 'min' => 3, 'max' => 255],
        ];
    }

    public function editUserData(){
        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        $user->username = $this->username;
        if(!empty($this->email))
        $user->email = $this->email;
        $user->avatar = array_pop(explode('/', $this->avatar));
        if(!empty($this->password))
        $user->password_hash = Password::hash($this->password);
        return $user->update(false);
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}