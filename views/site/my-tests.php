<?php

use common\models\database\Gallery;

$this->title = 'Корпорация Имиджа | Список тестов';
?>

<?= $this->render('/_block/_header'); ?>
<?= $this->render('/_block/_slider'); ?>

<section class="test-list no-pad female-lk pad-top-more head-marg">
    <ul class="test-tabs-wrap">
        <li class="my-test-tab current">мои тесты</li>
        <li class="new-test-tab">не пройденные тесты</li>
    </ul>
    <div class="container-main">
        <!--        finished tests -->
        <ul class="list-of-tests my-tests">
            <?php if ($finished_tests) : ?>
            <?php foreach ($finished_tests as $finished_test) : ?>
            <li>
                <div class="test-img list-item-0"
                     style="background-image: url(<?= Gallery::findOne(['id' => $finished_test['img']])->src ?>); background-size: cover;">
                    <div class="test-price">
                        <span class="new-price"><?= $finished_test['price'] ?></span>
                        <span class="currency-price">p</span>
                    </div>
                    <?php if ($finished_test['status'] == 'paid') : ?>
                        <!--                        пройденный и оплаченный тест-->
                        <a href="<?= Yii::$app->urlManager->createUrl(['/test/result', 'test' => $finished_test['id']]) ?>"
                           class="btn-go-test">Результат теста</a>
                    <?php else : ?>
                        <!--                        пройденный и неоплаченный тест-->
                        <a href="<?= Yii::$app->urlManager->createUrl(['/payment/invoice', 'test' => $finished_test['id']]) ?> "
                           class="btn-go-test">Результат теста</a>-->
                    <?php endif; ?>

                </div>

                <div class="disk-test">
                    <h3 class="list-title"><?= $finished_test['title'] ?></h3>
                    <p class="likes"><span class="likes-count"><?= $finished_test['like'] ?></span></p>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </li>
            <!-- end li-->

            <!--            <li>-->
            <!--                <div class="test-img list-item-2">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">300</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">Результат теста</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">лицо</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!--            <!-- end li-->
            <!--            <li>-->
            <!--                <div class="test-img list-item-1">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">300</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">Результат теста</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">стиль</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!-- end li-->
        </ul>
        <!--        unfinished tests -->
        <ul class="list-of-tests new-tests hide-tab">
            <?php if ($unfinished_tests) : ?>
                <?php foreach ($unfinished_tests as $unfinished_test) : ?>
                    <li>
                        <div class="test-img list-item-0"
                             style="background-image: url(<?= Gallery::findOne(['id' => $unfinished_test['img']])->src ?>); background-size: cover;">
                            <div class="test-price">
                                <!--                        <span class="old-price">399</span>-->
                                <span class="new-price"><?= $unfinished_test['price'] ?></span>
                                <span class="currency-price">p</span>
                            </div>

                            <a href="<?= Yii::$app->urlManager->createUrl(['/test/inittest', 'id' => $unfinished_test['id']]) ?>"
                               class="btn-go-test">ПРОЙТИ ТЕСТ</a>
                            <!--                            <a href="-->
                            <? //= Yii::$app->urlManager->createUrl(['/payment/invoice', 'test' => $unfinished_test['id']]) ?><!--" class="btn-go-test">ПРОЙТИ ТЕСТ</a>-->
                        </div>

                        <div class="disk-test">
                            <h3 class="list-title"><?= $unfinished_test['title'] ?></h3>
                            <p class="likes"><span class="likes-count"><?= $unfinished_test['like'] ?></span></p>
                        </div>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
            <!-- end li-->

            <!--            <li>-->
            <!--                <div class="test-img list-item-1">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">499</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">ПРОЙТИ ТЕСТ</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">Типы фигуры</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!--            <!-- end li-->
            <!--            <li>-->
            <!--                <div class="test-img list-item-2">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">1099</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">ПРОЙТИ ТЕСТ</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">Свадебные стили</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!--            <!-- end li-->
            <!--            <li>-->
            <!--                <div class="test-img list-item-2">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">1099</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">ПРОЙТИ ТЕСТ</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">Свадебные стили</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!--            <!-- end li-->
            <!--            <li>-->
            <!--                <div class="test-img list-item-2">-->
            <!--                    <div class="test-price">-->
            <!--                        <span class="new-price">1099</span>-->
            <!--                        <span class="currency-price">p</span>-->
            <!--                    </div>-->
            <!---->
            <!--                    <a href="#" class="btn-go-test">ПРОЙТИ ТЕСТ</a>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="disk-test">-->
            <!--                    <h3 class="list-title">Свадебные стили</h3>-->
            <!--                    <p class="likes"><span class="likes-count">327</span></p>-->
            <!--                </div>-->
            <!--            </li>-->
            <!-- end li-->
        </ul>
    </div>
</section>
<?= $this->render('/_block/_footer'); ?>
