<aside class="main-sidebar">
    <?php if ($advers) : ?>
        <?php for ($i = 0; $i < count($advers); $i++) : ?>
            <?php if ($advers[$i]['link'] != '') : ?>
                <div class="right-banner-<?= $i + 1 ?>">
            <a href="<?= $advers[$i]['link'] ?>" class="banner-link"><img
                    src="<?= Yii::getAlias('@advertising') ?>/<?= $advers[$i]['image'] ?>" alt="#"></a>
            </div>
            <?php endif; ?>
        <?php endfor; ?>
    <?php endif; ?>
</aside>