<?php
namespace frontend\controllers;

use common\models\database\Content;
use common\models\database\Gallery;
use common\models\database\Slider;
use Yii;
use common\models\database\Order;
use yii\web\Controller;
use common\models\database\Test;
use common\models\database\Blog;
use dektrium\user\models\LoginForm;
use dektrium\user\models\RegistrationForm;
use dektrium\user\models\User;
use frontend\models\UserComments;
use frontend\models\TestResults;
use frontend\models\Auth;
use yii\helpers\Url;
use common\models\database\User as Acc;
use common\models\main\DataOrder;
use frontend\models\Refer;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function actions()
    {
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
//        $session = Yii::$app->session;
//        var_dump($session['partner_id'] );
//        die();
        $request = Yii::$app->request;
        if ($request->get() != null) {
            $this->usePartner(key($request->get()));
        }
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
//        Yii::$app->view->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', ['depends' => 'yii\web\YiiAsset']);

        $model = Test::find()->orderBy('sort')->limit(3)->all();
        $slider = [];
        foreach (unserialize(Slider::findOne(['id' => 2])->image_id) as $key => $item) {
            $slider[$key] = Gallery::findOne(['id' => $item])->src;
        }
        $aboutUs = Content::findOne(['id' => 1]);
//        var_dump($aboutUs);die;
        //meta
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $aboutUs->getAttribute('meta_description')]);
        Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $aboutUs->getAttribute('meta_title')]);
        Yii::$app->view->registerMetaTag(['name' => 'keys', 'content' => $aboutUs->getAttribute('meta_keys')]);
        $blog = Blog::find()->limit(3)->all();
        $comments = UserComments::find()
            ->where(['status' => 1])
            ->orderBy('id DESC')
            ->all();
        return $this->render('index', [
            'model' => $model,
            'slider' => $slider,
            'comments' => $comments,
            'content' => [$aboutUs],
            'blog' => $blog
        ]);
    }

    public function actionMyTests()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        if (!Yii::$app->user->isGuest) {
            $temp_user_ids_tests = TestResults::selectResults();
            $temp_user_ids_paid_tests = $this->selectIdForPaidOrders();
            $user_ids_tests = $temp_user_ids_tests ? array_column($temp_user_ids_tests, 'id_test') : [];
            $user_ids_paid_tests = $temp_user_ids_paid_tests ? array_unique(array_column($temp_user_ids_paid_tests, 'for')) : [];
            unset($temp_user_ids_paid_tests);
            unset($temp_user_ids_tests);
            $user_tests = $this->selectUserTests($user_ids_tests);
            $finished_tests = array();
            $unfinished_tests = array();
            foreach ($user_tests as $user_test) {
                if (in_array($user_test['id'], $user_ids_tests)) {
//                if (in_array($user_test['id'], $user_ids_paid_tests))
                    if (in_array($user_test['id'], $user_ids_paid_tests))
                        $user_test['status'] = 'paid';
                    else
                        $user_test['status'] = 'unpaid';
                    array_push($finished_tests, $user_test);
                } else array_push($unfinished_tests, $user_test);
            }
            unset($user_tests);
            return $this->render('my-tests', [
                'finished_tests' => $finished_tests,
                'unfinished_tests' => $unfinished_tests,
            ]);
        } else {
            return $this->redirect(['site/index']);
        }
    }

    public function actionListTest()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        $female = Test::find()->where(['type' => 1])->orderBy(['sort' => SORT_DESC])->all();
        $male = Test::find()->where(['type' => 2])->orderBy(['sort' => SORT_DESC])->all();
        $wedd = Test::find()->where(['type' => 3])->orderBy(['sort' => SORT_DESC])->all();

        $seoBlock = Content::findOne(['id' => 2]);
        //meta
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $seoBlock->getAttribute('meta_description')]);
        Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $seoBlock->getAttribute('meta_title')]);
        Yii::$app->view->registerMetaTag(['name' => 'keys', 'content' => $seoBlock->getAttribute('meta_keys')]);

        return $this->render('list-test', [
            'female' => $female,
            'male' => $male,
            'wedd' => $wedd,
            'seoBlock' => $seoBlock,
        ]);
    }

    public function actionWeddTest()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        $wedd = Test::find()->where(['type' => 3])->orderBy(['sort' => SORT_DESC])->all();

        $seoBlock = Content::findOne(['id' => 2]);
        //meta
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $seoBlock->getAttribute('meta_description')]);
        Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $seoBlock->getAttribute('meta_title')]);
        Yii::$app->view->registerMetaTag(['name' => 'keys', 'content' => $seoBlock->getAttribute('meta_keys')]);

        return $this->render('wedd-test', [
            'wedd' => $wedd,
            'seoBlock' => $seoBlock,
        ]);
    }

    public function actionMaleTest()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        $male = Test::find()->where(['type' => 2])->orderBy(['sort' => SORT_DESC])->all();

        $seoBlock = Content::findOne(['id' => 2]);
        //meta
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $seoBlock->getAttribute('meta_description')]);
        Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $seoBlock->getAttribute('meta_title')]);
        Yii::$app->view->registerMetaTag(['name' => 'keys', 'content' => $seoBlock->getAttribute('meta_keys')]);

        return $this->render('male-test', [
            'male' => $male,
            'seoBlock' => $seoBlock,
        ]);
    }

    public function actionFemaleTest()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        $female = Test::find()->where(['type' => 1])->orderBy(['sort' => SORT_DESC])->all();

        $seoBlock = Content::findOne(['id' => 2]);
        //meta
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $seoBlock->getAttribute('meta_description')]);
        Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $seoBlock->getAttribute('meta_title')]);
        Yii::$app->view->registerMetaTag(['name' => 'keys', 'content' => $seoBlock->getAttribute('meta_keys')]);

        return $this->render('female-test', [
            'female' => $female,
            'seoBlock' => $seoBlock,
        ]);
    }

//    payment function for registered user

    public function actionPaySystem()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->isPost) {

            $acc = Acc::findOne(['email' => Yii::$app->user->identity->email]);

            $order = new DataOrder();
//        if ($order->validate()) {
            $order->getNewOrder($acc->id);
//            TestResults::addIdOrder(Yii::$app->request->post('test'), $order->order_id);
//        }

            $this->redirect(['/payment/one-pay', 'test' => Yii::$app->request->post('test'), 'order' => $order->order_id]);
        }
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        $session = Yii::$app->session;
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'id_source' => $attributes['id'] ? $attributes['id'] : $attributes['user_id'],
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                Yii::$app->user->login($user, 3600 * 24 * 30);
                if ($session['auth_url'] == 'invoice') {
                    $session['auth_url'] = '';
                    return $this->redirect(['/payment/invoice', 'test' => $session->get('current_test')]);
                }
            } else {
                switch ($client->getId()) {
                    case 'facebook':
                        if (User::findOne(['username' => explode(' ', $attributes['name'])[0]])) {
                            $name = explode(' ', $attributes['name'])[0] . '-' . rand(00, 99);
                        } else {
                            $name = (string)explode(' ', $attributes['name'])[0];
                        }
                        $user = new User([
                            'username' => $name,
                            'email' => 'default' . '_' . time(),
                            'password' => Yii::$app->security->generateRandomString(6),
                            'auth_key' => Yii::$app->security->generateRandomString(),
                        ]);
                        if ($user->save(false)) {
                            $auth = new Auth([
                                'id_user' => $user->id,
                                'source' => $client->getId(),
                                'id_source' => (string)$attributes['id'],
                            ]);
                            if ($auth->save()) {
                                Yii::$app->user->login($user, 3600 * 24 * 30);
                                if (isset($session['partner_id'])) {
                                    $refer = new Refer();
                                    $refer->addNewUserIdToPartner(Yii::$app->user->identity->id, $session['partner_id']);
                                }
                                if ($session['auth_url'] == 'invoice') {
                                    $session['auth_url'] = '';
                                    return $this->redirect(['/payment/invoice', 'test' => $session->get('current_test')]);
                                }
                            } else
                                print_r($auth->getErrors());
                        } else {
                            print_r($user->getErrors());
                        }
                        break;
                    case 'twitter':
                        if (User::findOne(['username' => explode(' ', $attributes['name'])[0]])) {
                            $name = explode(' ', $attributes['name'])[0] . '-' . rand(00, 99);
                        } else {
                            $name = (string)explode(' ', $attributes['name'])[0];
                        }
                        $user = new User([
                            'username' => $name,
                            'email' => 'default' . '_' . time(),
                            'password' => Yii::$app->security->generateRandomString(6),
                            'auth_key' => Yii::$app->security->generateRandomString(),
                        ]);
                        if ($user->save(false)) {
                            $auth = new Auth([
                                'id_user' => $user->id,
                                'source' => $client->getId(),
                                'id_source' => $attributes['id_str'],
                            ]);
                            if ($auth->save()) {
                                Yii::$app->user->login($user, 3600 * 24 * 30);
                                if (isset($session['partner_id'])) {
                                    $refer = new Refer();
                                    $refer->addNewUserIdToPartner(Yii::$app->user->identity->id, $session['partner_id']);
                                }
                                if ($session['auth_url'] == 'invoice') {
                                    $session['auth_url'] = '';
                                    return $this->redirect(['/payment/invoice', 'test' => $session->get('current_test')]);
                                }
                            } else
                                print_r($auth->getErrors());
                        } else {
                            print_r($user->getErrors());
                        }
                        break;
                    case 'vk':
                        if (User::findOne(['username' => $attributes['first_name']])) {
                            $name = $attributes['first_name'] . '-' . rand(00, 99);
                        } else {
                            $name = $attributes['first_name'];
                        }
                        $user = new User([
                            'username' => $name,
                            'email' => 'default' . '_' . time(),
                            'password' => Yii::$app->security->generateRandomString(6),
                            'auth_key' => Yii::$app->security->generateRandomString(),
                        ]);
                        if ($user->save(false)) {
                            $auth = new Auth([
                                'id_user' => $user->id,
                                'source' => $client->getId(),
                                'id_source' => (string)$attributes['user_id'],
                            ]);
                            if ($auth->save()) {
                                Yii::$app->user->login($user, 3600 * 24 * 30);
                                if (isset($session['partner_id'])) {
                                    $refer = new Refer();
                                    $refer->addNewUserIdToPartner(Yii::$app->user->identity->id, $session['partner_id']);
                                }
                                if ($session['auth_url'] == 'invoice') {
                                    $session['auth_url'] = '';
                                    return $this->redirect(['/payment/invoice', 'test' => $session->get('current_test')]);
                                }
                            } else
                                print_r($auth->getErrors());
//                            
                        } else {
                            print_r($user->getErrors());
                        }
                        break;
                }
            }


//            if (!empty($user)) {
//                Yii::$app->user->login($user);
//            } else {
//                $session = Yii::$app->session;
//                $session['attributes'] = $attributes;
//                $this->successUrl = Url::to('signup');
//            }
        }

    }

    public function actionNewUser()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if (User::findOne(['email' => $request->post('email')])) {
            $data['el'] = 'email';
            $data['error'] = 'Пользователь с таким email уже существует!';
            $data = json_encode($data);
            return $data;
        } elseif (User::findOne(['username' => $request->post('username')])) {
            $data['el'] = 'username';
            $data['error'] = 'Пользователь с таким именем уже существует!';
            $data = json_encode($data);
            return $data;
        } else {
            $user = new User([
                'username' => $request->post('username'),
                'email' => $request->post('email'),
                'password' => $request->post('password'),
                'auth_key' => Yii::$app->security->generateRandomString(),
            ]);
            if ($user->save(false)) {
                Yii::$app->user->login($user, 3600 * 24 * 30);
                if (isset($session['partner_id'])) {
                    $refer = new Refer();
                    $refer->addNewUserIdToPartner(Yii::$app->user->identity->id, $session['partner_id']);
                }
                $data['el'] = null;
                $data['error'] = '';
                $data = json_encode($data);
                return $data;
            }
        }
    }


    private function usePartner($partner)
    {
        $partner_id = User::findOne(['username' => $partner]);
        if ($partner_id) {
            $session = Yii::$app->session;
            $session['partner_id'] = $partner_id->id;
            unset($partner);
        }
    }

    private function selectUserTests($ids_tests)
    {
        return Test::find()
//            ->where(['in', 'id', $ids_tests])
            ->asArray()
            ->all();
    }

    private function selectIdForPaidOrders()
    {
        return Order::find()
            ->select(['for'])
            ->where(['id_user' => Yii::$app->user->identity->id])
            ->andWhere(['step' => 2])
            ->asArray()
            ->all();
    }
}
