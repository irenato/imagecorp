<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 02.08.2016
 * Time: 13:15
 */

namespace frontend\widgets;

use frontend\controllers\SiteController;
use Yii;
use yii\web\Response;
use dektrium\user\models\User;
use yii\web\NotFoundHttpException;
use dektrium\user\models\ResendForm;
use dektrium\user\models\LoginForm;
use common\models\main\DataOrder;
use yii\filters\AccessControl;

class LoginWidget extends \yii\bootstrap\Widget
{
    public function init()
    {
    }

    public function run()
    {
        if (!Yii::$app->user->isGuest) {
//           return $this->refresh();
    }
        $model = Yii::createObject(LoginForm::className());
//        $this->performAjaxValidation($model);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate() && $model->login()) {
            SiteController::refresh();
        }
        return $this->render('login/view', [
            'login_model'  => $model,
        ]);
    }

}