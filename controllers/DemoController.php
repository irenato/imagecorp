<?php

namespace frontend\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use common\models\database\TestValues;
use common\models\database\Test;

class DemoController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if ($action->id === 'result') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionResult()
    {
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
        Yii::$app->view->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', ['depends' => 'yii\web\YiiAsset']);
        $request = Yii::$app->request;
        if ($request->get('id') != null && $request->get('view') == 'demo') {
            $test_value = TestValues::find()
                ->select(['page_description', 'query_colors', 'query_values'])
                ->where(['id' => (int)$request->get('id')])
                ->asArray()
                ->one();
            $tests = Test::find()->orderBy('sort')->limit(3)->all();
            $box_color = ['белый' => 'color-10', 'синий' => 'color-12', 'голубой' => 'color-2', 'фиолетовый' => 'color-13', 'серый' => 'color-11', 'розовый' => 'color-9', 'бежевый' => 'color-1', 'черный' => 'color-8', 'желтый' => 'color-3', 'зеленый' => 'color-4', 'красный' => 'color-6', 'коричневый' => 'color-5', 'оранжевый' => 'color-7', 'мультиколор' => 'color-pick1'];
            $merchants = json_decode('[{"_id":72245,"name":"quelle.ru"},{"_id":70585,"name":"wildberries.ru"},{"_id":42071,"name":"lamoda.ru"},{"_id":50803,"name":"laredoute.ru"},{"_id":60347,"name":"otto.ru"},{"_id":73946,"name":"snowqueen.ru"},{"_id":72777,"name":"blackstarshop.ru"},{"_id":60374,"name":"butik.ru"},{"_id":73911,"name":"westland.ru"},{"_id":23631,"name":"mexx.ru"},{"_id":61005,"name":"tom-tailor-online.ru"},{"_id":67219,"name":"conceptclub.ru"},{"_id":69661,"name":"dcrussia.ru"},{"_id":71043,"name":"finn-flare.ru"},{"_id":76150,"name":"ru.puma.com"},{"_id":75625,"name":"tvoe.ru"}]');
            $model = new Test;
            $query_sort = $model->sort(\Yii::$app->request->queryParams);
            $count = 100;
            if ($query_sort['color'] != '' || $query_sort['merchant'] != '') {
                if ($query_sort['color'] != '') {
                    $query = '(' . implode(' OR ', explode(',', $test_value['query_values'])) . ') OR (' . $query_sort['color'] . ')';
                    $url = "http://api.gdeslon.ru/api/search.xml?q=" . $query . "&l=" . $count . "&p=1&_gs_at=d6c372061a5f4acafe310c1a9a2bbbfbfff0a3f6";
                } else if ($query_sort['merchant'] != '') {
                    $query = implode(' OR ', explode(',', $test_value['query_values']));
                    $url = "http://api.gdeslon.ru/api/search.xml?q=" . $query . "&m=" . $query_sort['merchant'] . "&l=" . $count . "&p=1&_gs_at=d6c372061a5f4acafe310c1a9a2bbbfbfff0a3f6";
                } else if ($query_sort['color'] != '' && $query_sort['merchant'] != '') {
                    $query = '(' . implode(' OR ', explode(',', $test_value['query_values'])) . ') OR (' . $query_sort['color'] . ')';
                    $url = "http://api.gdeslon.ru/api/search.xml?q=" . $query . "&m=" . $query_sort['merchant'] . "&l=" . $count . "&p=1&_gs_at=d6c372061a5f4acafe310c1a9a2bbbfbfff0a3f6";
                }
            } else {
                if ($test_value['query_colors'] != '') {
                    $query_colors = implode(' OR ', explode(',', str_replace(' ', '', strtolower($test_value['query_colors']))));
                    $query = '(' . implode(' OR ', explode(',', $test_value['query_values'])) . ') OR (' . $query_colors . ')';
                    $url = "http://api.gdeslon.ru/api/search.xml?q=" . $query . "&l=" . $count . "&p=1&_gs_at=d6c372061a5f4acafe310c1a9a2bbbfbfff0a3f6";
                } else {
                    $result = explode(',', $test_value['query_values']);
                    $query = implode(' OR ', array_slice($result, count($result) + 1));
                    $url = "http://api.gdeslon.ru/api/search.xml?q=" . $query . "&l=" . $count . "&p=1&_gs_at=d6c372061a5f4acafe310c1a9a2bbbfbfff0a3f6";
                }
            }
            $xml = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA);
            $offers = $xml->shop->offers;
            $data = [];
            foreach ($offers->offer as $offer) {
                $data[] = $this->Xml2Array($offer);
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => [
                    'pageSize' => 12,
                ],
            ]);
            return $this->render('result', [
                'test_value' => $test_value,
                'tests' => $tests,
                'box_color' => $box_color,
                'merchants' => $merchants,
                'dataProvider' => $dataProvider,
                'searchModel' => $model,
            ]);
        } else {
            return $this->redirect(['site/index']);
        }
    }

    protected function Xml2Array($xmlObject){
        $out = array();
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ?  $this->Xml2Array($node) : $node;
        return $out;
    }
}

