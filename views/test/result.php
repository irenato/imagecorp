<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\data\Pagination;
use common\models\database\Discount;
use common\models\database\Gallery;
use common\models\database\Slider;

$this->title = 'Корпорация Имиджа | Результат тестов';
/* @var $resulrt array */?>

<?= $this->render('/_block/_header'); ?>
<?= $this->render('/_block/_slider'); ?>


<!--block tests-->
<?= frontend\widgets\TestsWidget::widget() ?>



<?php foreach ($result as $res): ?>
        <?php
    $cur_colors = [];
    if ($res['result']['query_colors'] != '') {
        $cur_colors = explode(',', str_replace(' ', '', $res['result']['query_colors']));
    }
    $box_color = ['белый' => 'color-10', 'синий' => 'color-12', 'голубой' => 'color-2', 'фиолетовый' => 'color-13', 'серый' => 'color-11', 'розовый' => 'color-9', 'бежевый' => 'color-1', 'черный' => 'color-8', 'желтый' => 'color-3', 'зеленый' => 'color-4', 'красный' => 'color-6', 'коричневый' => 'color-5', 'оранжевый' => 'color-7', 'мультиколор' => 'color-pick1'];
    $merchants = json_decode('[{"_id":72245,"name":"quelle.ru"},{"_id":70585,"name":"wildberries.ru"},{"_id":42071,"name":"lamoda.ru"},{"_id":50803,"name":"laredoute.ru"},{"_id":60347,"name":"otto.ru"},{"_id":73946,"name":"snowqueen.ru"},{"_id":72777,"name":"blackstarshop.ru"},{"_id":60374,"name":"butik.ru"},{"_id":73911,"name":"westland.ru"},{"_id":23631,"name":"mexx.ru"},{"_id":61005,"name":"tom-tailor-online.ru"},{"_id":67219,"name":"conceptclub.ru"},{"_id":69661,"name":"dcrussia.ru"},{"_id":71043,"name":"finn-flare.ru"},{"_id":76150,"name":"ru.puma.com"},{"_id":75625,"name":"tvoe.ru"}]');
//    print_r($res);die();
    ?>         
   
                <!-- start about-user-->
    <section class="about-user">
                  
            <?= $res['result']['page_description'] ?>
   
    </section>
 
    <?php endforeach; ?>
    <!-- end about-user-->
    <!-- start product-recommendation -->
 <section class="product-recommendation">
        <div class="container-main">
            <h2 class="section-title">КОРПОРАЦИЯ ИМИДЖА РЕКОМЕНДУЕТ ВАМ</h2>
           
            <div class="wrap-product">
               
                <aside class="product-filter">
                    <p class="filter-title">Цвет</p>
                    <ul class="color-list">
                        <?php if ($res['result']['query_colors'] != '') { ?>
                        <?php foreach ($box_color as $key=>$value): ?>
                        <?php if (in_array($key, $cur_colors)):?>
                       
                        <li class="<?=$value ?>">
                            <input type="checkbox" id="<?= $value ?>" <?php if(!empty($_GET['Test']))if(substr_count($_GET['Test']['color'][0],$key)!=FALSE)  echo 'checked'; ?> data-value="<?= $key ?>">
                            <label for="<?= $value ?>" class="<?= $value ?>"></label>
                        </li>                        
                        <?php endif; ?>
                        <?php endforeach; ?>
                        <?php } else { ?>
                        <?php foreach ($box_color as $key=>$value): ?>                       
                        <li class="<?=$value ?>">
                            <input type="checkbox" id="<?= $value ?>" <?php if(!empty($_GET['Test']))if(substr_count($_GET['Test']['color'][0],$key)!=FALSE)  echo 'checked'; ?> data-value="<?= $key ?>">
                            <label for="<?= $value ?>" class="<?= $value ?>"></label>
                        </li>
                        <?php endforeach; ?>
                        <?php } ?>
                    </ul>
                    <p class="filter-title">Производители</p>
                    <ul class="brand-list brand-list-style" >
                        <?php // print_r($merchants[0]->name);die;
                        foreach($merchants as $num=>$merchant):?>
                        <li>
                            <input type="checkbox"  data-value="<?= $merchant->_id; ?>" id="brand_<?= $num; ?>">
                            <label for="brand_<?= $num; ?>" class="seller-label"><?= ($merchant->name)?$merchant->name:''; ?></label>
                        </li>
                        <?php endforeach; ?>
                       
                    </ul>
                    <button type="button" class="filter-btn" style="display:inline-block; top:0px;     position: relative;">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             width="20px" height="20px" fill="#fff" viewBox="0 0 402.577 402.577" style="enable-background: #fff;"
                                             xml:space="preserve">
                        <g>
                            <path d="M400.858,11.427c-3.241-7.421-8.85-11.132-16.854-11.136H18.564c-7.993,0-13.61,3.715-16.846,11.136
                                c-3.234,7.801-1.903,14.467,3.999,19.985l140.757,140.753v138.755c0,4.955,1.809,9.232,5.424,12.854l73.085,73.083
                                c3.429,3.614,7.71,5.428,12.851,5.428c2.282,0,4.66-0.479,7.135-1.43c7.426-3.238,11.14-8.851,11.14-16.845V172.166L396.861,31.413
                                C402.765,25.895,404.093,19.231,400.858,11.427z"/>
                        </g>
                    </svg>
                </button>                
                </aside>   
                <?php Pjax::begin(['timeout' => 5000]); ?>
                  
                    <?php $form = ActiveForm::begin(['id' => 'filter-form', 'action' => ['result', 'test' => $_GET['test']], 'method' => 'get', 'options' => ['data-pjax' => TRUE]]); ?>
                    <?= $form->field($searchModel, 'color[]')->hiddenInput()->label(FALSE); ?>
                    <?= $form->field($searchModel, 'merchant[]')->hiddenInput()->label(FALSE); ?>
                    
                    <?php ActiveForm::end(); ?>  
                <ul class="product-list">                    
                    <?php foreach ($dataProvider->getModels() as $product) : ?>
                        <li class="product-item">
                            <div class="wrap-img">
                                <?php // print_r($product['original_picture']); die(); ?>
                                <img height="220px" width="170px"src="<?php print_r($product['original_picture']) ; ?>" alt="#">
                                <span class="sale-label">-50%</span>
                                <button type="button" class="add-product-btn"></button>
                            </div>
                            <div class="product-disc">
                                <span class="name"><a href="<?= $product['url']; ?>"><?= $product['name']; ?></a></span>
                                <span class="price"><?= $product['price']; ?> <?= $product['currencyId']; ?>.</span>
                                <span class="old-price">-.</span>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
               <?php 
                    echo LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                    ]);
                    ?>
                  <?php Pjax::end(); ?>
            </div>
             
        </div>
    </section>
<?= $this->render('/_block/_footer'); ?>
