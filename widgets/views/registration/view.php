<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<!-- register modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="registration"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="register-modal" style="display: block">
        <div class="modal-content">

            <form id="form-registration" action="#">
                <input type="text" name="username" placeholder="Имя">
                <div><span class="text-danger registration-username"></span></div>
                <div><span class="text-success registration-username"></span></div>
                <input type="text" name="email" placeholder="E-mail">
                <div><span class="text-danger registration-email"></span></div>
                <div><span class="text-success registration-email"></span></div>
                <input type="password" name="password" placeholder="Пароль">
                <div><span class="text-danger registration-password"></span></div>
                <div><span class="text-success registration-password"></span></div>
                <button id="register-button" class="no-marg" type="submit">РЕГИСТРАЦИЯ</button>
            </form>

<!--            --><?php //$form = ActiveForm::begin([
//                'id'                     => 'registration-form',
//                'enableAjaxValidation' => true,
//                'enableClientValidation' => true,
//                'validateOnBlur' => true,
//                'validateOnType' => true,
//                'validateOnChange' => true,
//            ]); ?>
<!--            --><?//= $form->field($form_model, 'username', [
//                'inputOptions' => [
//                    'placeholder' => 'Имя',
//                ],
//            ])->label(false) ?>
<!--                <input type="tel" placeholder="Телефон">-->
<!--            --><?//= $form->field($form_model, 'email', [
//                'inputOptions' => [
//                    'placeholder' => 'E-mail',
//                ],
//            ])->label(false) ?>
<!--            --><?php //if ($module->enableGeneratingPassword == false): ?>
<!--                --><?//= $form->field($form_model, 'password', [
//                    'inputOptions' => [
//                        'placeholder' => 'Пароль',
//                    ],
//                ])->passwordInput()->label(false) ?>
<!--            --><?php //endif ?>
<!--            --><?//= Html::submitButton(Yii::t('user', 'РЕГИСТРАЦИЯ'), [
//                'id' => 'registration-button',
//                'class' => 'btn btn-success btn-block',
//            ]) ?>

<!--            --><?php //ActiveForm::end(); ?>

            <span class="info-text">C помощью аккаунта в соц. сетях</span>

            <ul class="login-by-soc">
                <li><a class="soc-item" href="/site/auth/*?authclient=twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a class="soc-item" href="/site/auth/*?authclient=facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a class="soc-item" href="/site/auth/*?authclient=vk"><i class="fa fa-vk"></i></a></li>
            </ul>
            <span class="info-text">Есть аккаунт? <a data-toggle="modal" id="callloginmodal" data-target="#login" href="#">Войти</a></span>
        </div>
    </div>
</div>
<!-- end register modal -->