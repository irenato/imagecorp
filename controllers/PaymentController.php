<?php

namespace frontend\controllers;

use common\models\database\Order;
use common\models\database\User;
use common\models\main\DataOrder;
use Yii;
use common\models\database\Test;
use common\models\main\StringModel;
use yii\web\NotFoundHttpException;
use dektrium\user\models\ResendForm;
use dektrium\user\models\RegistrationForm;
use yii\filters\AccessControl;
use yii\db\Query;
use frontend\models\TestResults;
use frontend\models\ReferIncome;
use frontend\models\Refer;

class PaymentController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if ($action->id === 'check' || $action->id === 'aviso' || $action->id === 'success' || $action->id === 'wrong') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        if (Yii::$app->request->get()) {
            $model = Test::findOne(['id' => Yii::$app->request->get('test')]);
        } else {
            return $this->redirect(['/site/index']);
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionInvoice()
    {
        $session = Yii::$app->session;
        $session['auth_url'] = 'invoice';
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        if (Yii::$app->request->get()) {
            $model = Test::findOne(['id' => Yii::$app->request->get('test')]);
        } else {
            return $this->redirect(['/site/index']);
        }
        return $this->render('invoice', [
            'model' => $model
        ]);
    }

    public function actionOnePay()
    {
        $session = Yii::$app->session;
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        if (Yii::$app->request->get()) {
            $model = Test::findOne(['id' => Yii::$app->request->get('test')]);
        } else {
            return $this->redirect(['/site/index']);
        }

        $order = Order::findOne(['order_id' => Yii::$app->request->get('order')]);
        $user = User::findOne(['id' => $order->id_user]);
        $currentTest = $session->get('current_test');
        $result = $this->createResult($currentTest);
        if (TestResults::existsResult($currentTest)) {
            TestResults::updateResult($currentTest, json_encode($result), Yii::$app->request->get('order'));
        } else {
            $new_result = new TestResults();
            $new_result->saveResult($currentTest, json_encode($result), Yii::$app->request->get('order'));
        }
//        TestResults::addIdOrder(Yii::$app->request->post('test'), $order->order_id);

        $configs = array();
        $configs['shopId'] = '117233';
        $configs['scId'] = '45542';
//        $configs['scId'] = '532316';
        $configs['ShopPassword'] = 'asD15famil8476aoN';

        return $this->render('one-pay', [
            'model' => $model,
            'order' => $order,
            'configs' => $configs,
            'user' => $user,
        ]);
    }

    public function actionCheck()
    {
        $model = Order::findOne(['order_id' => Yii::$app->request->post('customerNumber')]);
        $configs = array();
        $configs['shopId'] = '117233';
        $configs['scId'] = '532316';
        $configs['ShopPassword'] = 'asD15famil8476aoN';
        $data = unserialize($model->data);
        $data['stepOne'] = $_POST;
        $model->data = serialize($data);
        $model->step = 1;
        $model->update();


        $f = fopen("uploads/check.txt", "w");
// Записать текст
        fwrite($f, serialize($_POST));
// Закрыть текстовый файл
        fclose($f);


        $hash = md5(Yii::$app->request->post('action') . ';' . Yii::$app->request->post('orderSumAmount') . ';' . Yii::$app->request->post('orderSumCurrencyPaycash') . ';' . Yii::$app->request->post('orderSumBankPaycash') . ';' . $configs['shopId'] . ';' . Yii::$app->request->post('invoiceId') . ';' . Yii::$app->request->post('customerNumber') . ';' . $configs['ShopPassword']);
        if (strtolower($hash) != strtolower(Yii::$app->request->post('md5'))) {
            $code = 1;
        } else {
            $code = 0;
        }
        Yii::$app->response->format = 'xmlCheck';
        $arr = [
            'performedDatetime' => date("c"),
            'code' => $code,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => $configs['shopId']
        ];
        return $arr;
    }

    public function actionAviso()
    {
        $model = Order::findOne(['order_id' => Yii::$app->request->post('customerNumber')]);
        $refer_income = new ReferIncome();
        $refer_income->saveIncome($model['id_user'], $model['for'], $model['sum']);
        $configs = array();
        $configs['shopId'] = '117233';
        $configs['scId'] = '532316';
        $configs['ShopPassword'] = 'asD15famil8476aoN';

        $data = unserialize($model->data);

        $data['stepTwo'] = $_POST;

        $model->data = serialize($data);
        $model->step = 2;
        $model->update();
        $hash = md5(Yii::$app->request->post('action') . ';' . Yii::$app->request->post('orderSumAmount') . ';' . Yii::$app->request->post('orderSumCurrencyPaycash') . ';' . Yii::$app->request->post('orderSumBankPaycash') . ';' . $configs['shopId'] . ';' . Yii::$app->request->post('invoiceId') . ';' . Yii::$app->request->post('customerNumber') . ';' . $configs['ShopPassword']);
        if (strtolower($hash) != strtolower(Yii::$app->request->post('md5'))) {
            $code = 1;
        } else {
            $code = 0;
        }
        Yii::$app->response->format = 'xmlAviso';
        $arr = [
            'performedDatetime' => date("c"),
            'code' => $code,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => $configs['shopId']
        ];
        return $arr;
    }

    public function actionSuccess()
    {
        $request = Yii::$app->request;
        $test_result_id = TestResults::find()
            ->select(['id_test'])
            ->where(['id_order' => $request->get('orderNumber')])
            ->asArray()
            ->one();
        return $this->redirect(['/test/result', 'test' => $test_result_id['id_test']]);
    }

    public function actionWrong()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        $meta = [
            'http-equiv' => 'Refresh',
            'content' => '5; url=https://imagecorp.ru',
        ];
        \Yii::$app->view->registerMetaTag($meta);
        return $this->render('wrong');
    }

    public function actionTestError()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        $meta = [
            'http-equiv' => 'Refresh',
            'content' => '5; url=https://imagecorp.ru/site/list-test',
        ];
        \Yii::$app->view->registerMetaTag($meta);
        return $this->render('test-error');
    }

    protected function createResult($test)
    {
        $session = Yii::$app->session;
        $tests = $session->get('tests');
        $current_test = $session->get('current_test');

        if ($session->get('current_test') != $test || !$current_test) {
            return $this->redirect(['test', 'number' => $session->get('passed_questions', 0)]);
        }

        $resultRender = [];
        foreach ($tests[$current_test]['tests'] as $testResult) {
            $testModel = $this->findModel($testResult['test_id']);

            if ($testModel->getAttribute('result_type_id') == 1) {
                $answewrs = $testResult['answewrs'];

                $result = 0;
                foreach ($answewrs as $answer) {
                    $result += $answer;
                }

                $query = new Query;
                $query->select('answer, query_values, query_colors, page_title, page_description')->from('test_values')->where(['and', "`from`<=$result", "`to`>=$result"])->andWhere(['test_id' => $testResult['test_id']]);

                $resultQuery = $query->one();
                $resultRender[$testResult['test_id']]['result'] = $resultQuery;
                $resultRender[$testResult['test_id']]['title'] = $testModel->getAttribute('title');
            } elseif ($testModel->getAttribute('result_type_id') == 2) {

                $answewrs = $testResult['answewrs'];

                $answewrsNumbersByid = $testResult['answewrsid'];

                $query = new Query;
                $query->select(' * ')->from('test_values_matrix')->where(['test_id' => $testResult['test_id']])->andWhere(['active_flag' => 1]);
                $result = $query->one();

                if (!$result) {
                    return $this->redirect(['payment/test-error']);

                }
                    //get matrix
                    $matrix = unserialize($result['serialize']);
                    $testValueId = $matrix[$answewrs[$answewrsNumbersByid[$result['question_vertical_id']]]][$answewrs[$answewrsNumbersByid[$result['question_horizontal_id']]]];

                    if (!$testValueId) {
                        return $this->redirect(['payment/test-error']);
                    }

                    $query = new Query;
                    $query->select('answer, query_values, query_colors, page_title, page_description')->from('test_values')->where(['id' => $testValueId]);
                    $resultQuery = $query->one();
                    $resultRender[$testResult['test_id']]['result'] = $resultQuery;
                    $resultRender[$testResult['test_id']]['title'] = $testModel->getAttribute('title');

            } else {
                $answewrs = $testResult['answewrs'];
                $resultArr = [];
                foreach ($answewrs as $answer) {
                    if (!isset($resultArr[$answer])) {
                        $resultArr[$answer] = 0;
                    }
                    $resultArr[$answer]++;
                }

                asort($resultArr);
                end($resultArr);
                $result = key($resultArr);

                $query = new Query;
                $query->select('answer, query_values, query_colors, page_title, page_description')->from('test_values')->where(['from' => $result])->andWhere(['test_id' => $testResult['test_id']]);

                $resultQuery = $query->one();
                $resultRender[$testResult['test_id']]['result'] = $resultQuery;
                $resultRender[$testResult['test_id']]['title'] = $testModel->getAttribute('title');
            }
        }
        return $resultRender;
    }

    protected function findModel($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}