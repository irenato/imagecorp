<?php

namespace frontend\widgets;

use common\models\database\Test;


class TestsWidget extends \yii\bootstrap\Widget
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function init()
    {
    }

    public function run()
    {
        $ids = ['3', '4', '19'];
        $tests = Test::find()
            ->where(['in', 'id', $ids])
            ->orderBy('id')
            ->all();
        return $this->render('tests/view', [
            'tests' => $tests,
        ]);
    }
}