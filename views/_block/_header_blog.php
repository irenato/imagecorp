<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\database\BlogCategory;

?>
<header class="blog-header blog-container">
    <div class="top-row">
        <div class="test-link">
            <a href="<?= Yii::$app->urlManager->createUrl(['site/list-test']) ?>" class="head-text hover-prime">Тесты по имиджу</a>
        </div>
        <div class="login-menu">
            <?php
            if (!Yii::$app->user->isGuest) {
                echo '<ul class="user-menu">';
                echo '<li><a href="' . Yii::$app->urlManager->createUrl(['dashboard/index']) . '">Профиль</a></li>';
                echo '<li><a href="' . Yii::$app->urlManager->createUrl(['site/my-tests']) . '">Мои тесты</a></li>';
                echo '<li>' . $url = Html::a('Выйти',
                    ['//user/security/logout'],
                    ['data-method' => 'post']) . '</li>';
                echo '</ul>';
                echo '<div class="login-menu">';
                echo '<img src="/' . Yii::getAlias('@avatar') . '/' . Yii::$app->user->identity->avatar . '" alt="">';
//                if (Yii::$app->controller->route == 'site/index') {
                    echo '<p class="login-btn">' . Yii::$app->user->identity->username . '</p>';
//                } else {
//                    echo '<p class="login-btn style-color">' . Yii::$app->user->identity->username . '</p>';
//                }
            } else {
                echo '<ul class="user-menu">';
                echo '<li><a data-toggle="modal" data-target="#login" href="#">Войти</a></li>';
                echo '<li><a data-toggle="modal" data-target="#registration" href="#">Зарегистрироваться</a></li>';
                echo '</ul>';
                echo '<div class="login-menu">';
                echo '<div class="header-item">';
                if (Yii::$app->controller->route == 'site/index') {
                    echo '<p class="login-btn">Меню</p>';
                } else {
                    echo '<p class="login-btn style-color">Меню</p>';
                }
            }
            echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
    <div class="header-logo">
        <a href="/"><img src="/theme/img/logo_light.svg" alt="logo"></a>
    </div>
    <div class="header-nav">
        <div class="blog-link">
            <a href="<?= Url::toRoute(['/blog/index']); ?>" class="head-text hover-prime">БЛОГ</a>
        </div>
        <nav class="main-nav">
            <ul>
                <?php
                $blog_categories = BlogCategory::find()->orderBy(['sort' => SORT_ASC])->all();
                $i = 0;
                foreach ($blog_categories as $item) {
                    $i++;
                    echo '<li><a class="';
                    if (Yii::$app->request->get('category') == $item->category_alias) {
                        echo 'current-link ';
                    }
                    if ($i == 1)
                        echo 'img-link has-subcategory" href="' . Url::toRoute(['/blog/index', 'category' => $item->category_alias]) . '">' . $item->title . '</a></li>';
                    else
                        echo 'img-link" href="' . Url::toRoute(['/blog/index', 'category' => $item->category_alias]) . '">' . $item->title . '</a></li>';
                }
                ?>
            </ul>
        </nav>

        <div class="search">
            <form action="#">
                <input class="search-input" type="text">
                <button class="btn-search" type="submit"></button>
            </form>
        </div>
    </div>
</header>