<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
Yii::setAlias('@avatar', 'uploads/avatar/');
Yii::setAlias('@advertising', '/uploads/advertising/');
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/urlManager.php'),
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
//                'google' => [
//                    'class' => 'yii\authclient\clients\GoogleOpenId'
//                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    /** TODO Change clientId and clientSecret to actual */
                    'clientId' => '750481425055236',
                    'clientSecret' => '2cce494098894e40c7afcb6694c2bd9a',
                ],
                'vk' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    /** TODO Change clientId and clientSecret to actual */
                    'clientId' => '5572434',
                    'clientSecret' => 'eSHbXS3yXFIktgLtgHEj',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    /** TODO Change consumerKey and consumerSecret to actual */
                    'consumerKey' => '8JlJydX8iBZ4y07C4sxpdsP2L',
                    'consumerSecret' => 'qiy2UTY4nKz9xBP4RWdFSFzbGPYNPGTQzlIfYqn1HDn57LFfX7',
                ],
                /*'behance' => [
                    'class' => 'frontend\models\Behance',
                    /** TODO Change consumerKey and consumerSecret to actual */
                /*  'clientId' => 'q7MPeWXQxhsyB0KGhI9foUpbNslWZLym',
              ],*/
            ],
        ],

    ],

    'modules' => [
        'user' => [
            // following line will restrict access to admin controller from frontend application
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'controllerMap' => [
                'registration' => 'frontend\user\controllers\RegistrationController',
                'security' => 'frontend\user\controllers\SecurityController',
                'recovery' => 'frontend\user\controllers\RecoveryController',
            ],
        ],
    ],
    'params' => $params,
];
