<?php
/**
 * Created by PhpStorm.
 * User: Аркадий
 * Date: 13.08.2016
 * Time: 13:39
 */

namespace frontend\widgets;

use frontend\models\AdvertisingBlock;

class AdvertisingRightWidget extends \yii\bootstrap\Widget
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function init()
    {
    }

    public function run()
    {

        return $this->render('advertising-right/view',[
            'advers' => AdvertisingBlock::selectAdvers(),
        ]);
    }
}