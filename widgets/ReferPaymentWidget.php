<?php
/**
 * Created by PhpStorm.
 * User: Alscon
 * Date: 24.08.2016
 * Time: 16:26
 */

namespace frontend\widgets;


class ReferPaymentWidget extends \yii\bootstrap\Widget
{
    public function init()
    {
    }

    public function run()
    {


        return $this->render('refer-payment/view');
    }
}