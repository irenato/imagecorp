<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="modal fade" tabindex="-1" role="dialog" id="recovery"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="email-modal" style="display: block">
        <div class="modal-content">
            <form id="form-recovery" action="#">
                <input type="email" name="email" placeholder="Введите e-mail">
                <div><span class="text-danger"></span></div>
                <div><span class="text-success"></span></div>
                <button id="recovery-button" class="no-marg" type="submit">ПРОДОЛЖИТЬ</button>
            </form>
        </div>
    </div>
</div>
